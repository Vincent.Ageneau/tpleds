#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
int ledPrinc = 0;
int i = 0;

void setup()
{
  //configuration du ruban de LEDs
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void allumerLeds(int prmNumPrincipal)
{
  prmNumPrincipal = prmNumPrincipal + i;
  FastLED.clear();
  leds[(prmNumPrincipal - 1 + 10) % NUM_LEDS] = CHSV(150, 255, 50);
  leds[prmNumPrincipal % NUM_LEDS] = CHSV(150, 255, 255);
  leds[(prmNumPrincipal + 1) % NUM_LEDS] = CHSV(150, 255, 50);
  FastLED.show();
  i++;
}

void loop()
{
  // commander les LEDS
  allumerLeds(5);
  delay(750);
}
