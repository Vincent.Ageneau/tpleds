#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2 //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
int i = 0;
int e = 1;
int r = 2;

void setup()
{
//configuration du ruban de LEDs
FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void allumerLeds(int prmNumPrincipal){
  e=prmNumPrincipal;
  i=e-1;
  r=e+1;
}

void loop()
{
// commander les LEDS
  allumerLeds(1);
FastLED.clear();
leds[i] = CHSV(150, 255, 50);
leds[e] = CHSV(150, 255, 255);
leds[r] = CHSV(150, 255, 50);
FastLED.show();
i++;
e++;
r++;
delay (250);
if (i>9)
{
  i = 0;
}
if (e>9)
{
  e = 0;
}
if (r>9)
{
  r = 0;
}
}


