#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
int ledPrinc = 0;
int i = 0;
float r = 0;

void setup()
{
  //configuration du ruban de LEDs
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop()
{
  // commander les LEDS
  leds[i%NUM_LEDS] = CHSV(r, 255, 255);
  FastLED.show();
  i++;
  r=r+25.5;
  delay(10);
  if (r>255)
  {
    r=0;
  }
  
}
